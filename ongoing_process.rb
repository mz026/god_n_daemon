current_dir = File.expand_path(File.dirname(__FILE__))

File.open("#{current_dir}/dummy.log", 'a+') do |f|
  f.puts "#{Time.now} -- process start, version 3"
end

100000.times do |i|
  File.open("#{current_dir}/dummy.log", 'a+') do |f|
    f.puts "#{Time.now} -- #{i}"
    sleep 0.5
  end
end
