God testing project

- use `daemons` to daemonize `ongoing_process.rb`:
  - `daemons`: [here](http://rubydoc.info/gems/daemons/1.1.9) and [here](http://snippets.aktagon.com/snippets/212-how-to-create-a-daemon-process-using-ruby-and-the-daemons-rubygem)
  - start: `$ ruby control.rb start <app-name>`, which would start the daemon through the wrapping script `contol.rb`
  - stop: `$ ruby control.rb stop <app-name>`, which would stop the daemon

- use god to monitor the daemon process:
  1. start god by `$ god`
  2. load config by `$ god load config.god`
  3. restart group by `$ god restart <group name>`, where `group name` is `ongoing` in this project
